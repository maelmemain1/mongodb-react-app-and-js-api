# Magasin de Sport : tp final 
- Author : MEMAIN Maël

## Lancer l'app : 
- Méthode facile : 
    - Exécuter le fichier launch.sh dans le terminal comme ceci : 'bash launch.sh'
- Méthode manuelle : 
    - Lancer mongodb
    - Lancer l'API en executant : 'node server.js' dans /backend
    - Lancer l'app React en executant : 'npm run start' dans /appreact
    - Se connecter a localhost:3000 pour utiliser l'app react

## Fonctionnalités
- Méthodes CRUD...