const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const bodyParser = require("body-parser");

const app = express();
const port = 5001;

app.use(
  cors({
    origin: "http://localhost:3000",
  })
);

app.use(bodyParser.json());

// Connect to MongoDB
mongoose.connect("mongodb://localhost:27017/MagasinDeSport", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error:"));
db.once("open", () => {
  console.log("Connected to MongoDB");
});

// article
const articleSchema = new mongoose.Schema({
  marque: String,
  prix: Number,
  nom: String,
  référence: String,
  catégorie: String,
  tailles: [String],
  fournisseur: {
    nom: String,
    ville: String,
  },
  identifiant_rayon: String,
});

//rayon
const rayonSchema = new mongoose.Schema({
  rayon: String,
  description: String,
});

//achat
const achatSchema = new mongoose.Schema({
  articles: [
    {
      marque: String,
      prix: Number,
      nom: String,
    },
  ],
  prix_total: Number,
  client: String,
  date: String,
});

const Article = mongoose.model("Article", articleSchema);
const Rayon = mongoose.model("Rayon", rayonSchema);
const Achat = mongoose.model("Achat", achatSchema);

// articles route
app.get("/articles", async (req, res) => {
  try {
    const articles = await Article.find();
    res.json(articles);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

app.get("/articles/:id", async (req, res) => {
  try {
    const articles = await Article.findById(req.params);
    res.json(articles);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

app.post("/articles", async (req, res) => {
  const article = new Article(req.body);
  try {
    const newArticle = await article.save();
    res.status(201).json(newArticle);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

app.put("/articles/:id", async (req, res) => {
  const { id } = req.params;
  try {
    await Article.findByIdAndUpdate(id, req.body);
    res.status(200).json({ message: "Article updated successfully" });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

app.delete("/articles/:id", async (req, res) => {
  const { id } = req.params;
  try {
    await Article.findByIdAndDelete(id);
    res.status(200).json({ message: "Article deleted successfully" });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

app.get("/achats", async (req, res) => {
  try {
    const achats = await Achat.find();
    res.json(achats);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

app.post("/achats", async (req, res) => {
  const achat = new Achat(req.body);
  try {
    const newAchat = await achat.save();
    res.status(201).json(newAchat);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

app.put("/achats/:id", async (req, res) => {
  const { id } = req.params;
  try {
    await Achat.findByIdAndUpdate(id, req.body);
    res.status(200).json({ message: "Achat updated successfully" });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

app.delete("/achats/:id", async (req, res) => {
  const { id } = req.params;
  try {
    await Achat.findByIdAndDelete(id);
    res.status(200).json({ message: "Achat deleted successfully" });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

app.get("/rayons", async (req, res) => {
  try {
    const rayons = await Rayon.find();
    res.json(rayons);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

app.get("/rayons/:id", async (req, res) => {
  try {
    const rayons = await Rayon.findById(req.params);
    res.json(rayons);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

app.post("/rayons", async (req, res) => {
  const rayon = new Rayon(req.body);
  try {
    const newRayon = await rayon.save();
    res.status(201).json(newRayon);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

app.put("/rayons", async (req, res) => {
  const { id } = req.params;
  try {
    await Rayon.findByIdAndUpdate(id, req.body);
    res.status(200).json({ message: "Rayon updated successfully" });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

app.delete("/rayons/:id", async (req, res) => {
  const { id } = req.params;
  try {
    await Rayon.findByIdAndDelete(id);
    res.status(200).json({ message: "Rayon deleted successfully" });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

app.listen(port, () => {
  console.log(`Server is running on port: ${port}`);
});
